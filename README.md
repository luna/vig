# vig

a [v] parser in zig, also a shitpost taken too far

[v]: https://vlang.io

(will likely be done as a full compiler, who knows, leave a like and subscribe
for more epic adventures)

## why

 - because i want to learn parsers and what best to do it with a language i'm
negatively charged towards
 - theres an ast now it looks pretty
 - i finally understand recursive descent parsers

## variations

 - `for` is split between `for` and `loop` because my fucking god i cant stand
    having *four* different variations of `for` to parse.

 - struct initialization is with `Struct.{}`, not `Struct{}`, to remove parsing
    ambiguities (`if a {}` and `a{}`, v solves that with case, e.g structs Must
    Be Properly Titled and i can't bother with that)

 - function parameters must have a type always
    (no `fn blah(x, y int)`, must be `fn blah(x int, y int)`)
 - ifs are always statements, not expressions
    (can't use if as an r-value: `x := if ...`)
 - no `++`, `--` (use `+=` and `-=`)
 - no `match`
 - no embedded structs
 - no enum literals (`x = .val2`)
 - no v orm
 - no codegen
 - no inline assembly

## wip

 - no `map` yet
 - no `in` yet (probably will be dropped)
 - no `module`, `import` yet
 - no interfaces yet
 - no generics yet
 - no attributes yet (`[live]`, `[skip]`)
 - no `defer` yet
 - no `switch` yet
 - no operator overloading methods yet
 - do we really want a type system (optionals included)
    - no error handling yet (`call() or {blah}`)
    - no `?` postfix operator yet (`call()?`)
 - do we really want to output c
 - do we really want to output llvm
 - do we really want to output x86

## how

```
git clone https://gitdab.com/luna/vig.git
cd vig
zig build install --prefix ~/.local/
```
