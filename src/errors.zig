const std = @import("std");
pub fn report(line: usize, where: []const u8, message: []const u8) void {
    std.debug.warn("[line {}] Error{}: {}", .{ line, where, message });
}

pub fn reportN(line: usize, message: []const u8) void {
    report(line, "", message);
}

pub fn reportFmt(line: usize, comptime fmt: []const u8, args: anytype) void {
    std.debug.warn("[line {}] Error", .{line});
    std.debug.warn(fmt, args);
    std.debug.warn("\n", .{});
}
