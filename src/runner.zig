const std = @import("std");
const scanners = @import("scanner.zig");
const parsers = @import("parser.zig");
const main = @import("main.zig");
const ast = @import("ast.zig");

const tokens = @import("tokens.zig");
const printer = @import("ast_printer.zig");

const Allocator = std.mem.Allocator;
const Result = main.Result;
const Parser = parsers.Parser;

pub const Runner = struct {
    allocator: *Allocator,
    stdout: std.fs.File.OutStream,

    pub fn init(allocator: *Allocator, stdout: anytype) Runner {
        return .{ .allocator = allocator, .stdout = stdout };
    }

    fn testScanner(self: *Runner, scanner: *scanners.Scanner) !void {
        while (true) {
            var tok_opt = scanner.nextToken() catch |err| {
                try self.stdout.print("error at '{}': {}\n", .{
                    scanner.currentLexeme(),
                    err,
                });

                return Result.ScannerError;
            };

            if (tok_opt) |tok| {
                if (tok.ttype == .EOF) break;
                try self.stdout.print("{x}\n", .{tok});
            }
        }
    }

    pub fn execute(self: *Runner, code: []const u8) !void {
        var scanner = scanners.Scanner.init(self.allocator, code);
        try self.testScanner(&scanner);

        scanner = scanners.Scanner.init(self.allocator, code);
        var parser = Parser.init(self.allocator, &scanner);
        var root = try parser.parse();

        // var it = root.Root.iterator();

        std.debug.warn("parse tree\n", .{});
        printer.printNode(root, 0);

        return Result.Ok;
    }
};
